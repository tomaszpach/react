let courseMedia = function (data) {
    return <img src={data.image} alt="cover"/>
};

let newLabel = function (data) {
    return data.is_new ? <span className="badge badge-success">Nowy!</span> :
        <span className="badge badge-danger">Stary!</span>;
};

let coursePromoLabel = function (data) {
    return data.is_promo ? <b>Kurs jest w PROMOCJI!</b> : null;
};

let courseActions = function (data) {
    return (
        <div className="btn-group pull-right">
            <button className="btn btn-default"> Szczegóły kursu</button>
            <button className="btn btn-default"> Dodaj do ulubionych</button>
            <button className="btn btn-default"> Dodaj do koszyka</button>
        </div>
    );
};

let courseDetails = function (data) {
    return (
        <table className="media-right">
            <tbody className="table course_details">
            <tr>
                <th>Autor</th>
                <td>{data.author}</td>
            </tr>
            <tr>
                <th>Czas trwania</th>
                <td>{data.duration}</td>
            </tr>
            </tbody>
        </table>
    );
};

let Course = function (data) {
    return (
        {/* Kurs media */},
            <div className="course media">
                <div className="media-left">
                    {courseMedia(data)}
                </div>

                {/* Kurs body */}
                <div className="media-body px-3">
                    <h3>{data.title} {newLabel} </h3>
                    <p>{data.description}</p>

                    {/* Promocja */}
                    {coursePromoLabel(data)}

                    {/* Course actions */}
                    {courseActions(data)}


                </div>
                {/* Kurs details column */}
                {courseDetails(data)}
            </div>
    )
};

// Dynamiczna lista elementów do wyświetlenia. Pobiera X elementów
let CoursesList = function (list) {
    return (
        <div>
            {list.map(function (data) {
                // Dodanie unikatowego klucza. Unikamy problemów z późniejszymi zmianami w elementach
                return <div key={data.id}>{Course(data, data.id)}</div>
            })}
        </div>
    )
};

let list = courses_data.slice(0, 6);
// console.log(list);

ReactDOM.render(CoursesList(list), document.getElementById('root'));

// Wyświetlenie 3 elementów
// ReactDOM.render(<div>
//     {Course(courses_data[0])}
//     {Course(courses_data[1])}
//     {Course(courses_data[2])}
//     </div>, document.getElementById('root'));
// ReactDOM.render(course(courses_data[0]), document.getElementById('root'));