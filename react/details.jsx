var header = function (data) {
        return ( data.firstName + ' ' + data.lastName )
    },
    media = function(data) {
        return <img className="card-img-top" src={ data.image } alt="Card image cap" />
    },
    cardTitle = function(data) {
        return <h4 className="card-title"> { data.firstName + ' ' + data.lastName } </h4>
    },
    cardDetails = function(data) {
        return (
            <div className="details">
                { data.number ? <p className="card-text"><span className="badge badge-default">Numer telefonu:</span> { data.number } </p> : null }
                { data.email ? <p className="card-text"><span className="badge badge-default">Adres e-mail:</span> { data.email } </p> : null }
                { data.website ? <p className="card-text"><span className="badge badge-default">Strona www:</span> { data.website } </p> : null }
                { data.position ? <p className="card-text"><span className="badge badge-default">Stanowisko:</span> { data.position } </p> : null }
                { data.team ? <p className="card-text"><span className="badge badge-default">Drużyna:</span> { data.team } </p> : null }
                { data.skype ? <p className="card-text"><span className="badge badge-default">Skype:</span> { data.skype } </p> : null }
                { data.holidays ? <p className="card-text"><span className="badge badge-default">Wzięte wolne:</span> { data.holidays } </p> : null }
            </div>
        );
    };

var accordionID = function (data) {
    // data.is_promo ? <b>Kurs jest w PROMOCJI!</b> : null;
    return (
        <a data-toggle="collapse" data-parent="#accordion" href={ data.hash + data.firstName + data.id } aria-expanded="false" aria-controls={ data.hash + data.firstName + data.id }>
            { header(data) }
        </a>
    )
};


var Details = function (data) {
    return (
        <div id="accordion" role="tablist" aria-multiselectable="false">
            <div className="card">
                <div className="card-header" role="tab" id="headingOne">
                    <h5 className="mb-0">
                        { accordionID(data) }
                    </h5>
                </div>
                <div id={ data.firstName + data.id } className={ data.id === 0 ? "collapse" : "collapse" } role="tabpanel" aria-labelledby="headingOne">
                    <div className="card-block">
                        <div className="card">
                            { media(data) }
                            <div className="card-block">
                                { cardTitle(data) }
                                { cardDetails(data) }
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    );
};

var DetailList = function(list) {
    return (
        <div>
            {list.map(function(data) {
                return <div key={data.id}> {Details(data) }</div>
            })}
        </div>
    )
};

var list = data.slice(0, 11);

ReactDOM.render(DetailList(list), document.getElementById('root'));

ReactDOM.render(details, document.getElementById('details'));